<!DOCTYPE html>
<html>
<head>
  <title>PHP - Basic</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <div class="container">
    <h4>PHP Basic</h4>
    <?php
      echo "<p align='center'><font color='blue' size='5'>Ví dụ lệnh echo trong PHP!</font></p>";
      //hoặc
      print "<p align='center'><font color='red' size='5'>Ví dụ lệnh print trong PHP!</font></p>";

      print <<<EOF
        "print EOF" <br />
EOF;

      $bien_int = 12345;
      $bien_int_khac = -12345 + 12345;

      if (true)
        print("Dòng văn bản này luôn luôn được in trên trình duyệt.<br>");

      else
        print("Dòng văn bản này sẽ không bao giờ được in trên trình duyệt.<br>");

      $x = <<<EOF
      Ví dụ minh họa cách trích dẫn "dấu nháy" trong PHP!<br />
EOF;

      print $x;

      $bien_toan_cuc = 66;
   
      function hamDemo() {
          GLOBAL $bien_toan_cuc;
          $bien_toan_cuc++;
          
          print "<br/>Giá trị biến toàn cục là $bien_toan_cuc <br /><br />";
      }
      
      hamDemo();

      function ham_demo2() {
          STATIC $count = 3.14;
          $count++;
          print $count;
          print "<br />";
      }
      
      ham_demo2();
      ham_demo2();
      ham_demo2();
      print "<br />";

      define("VIETJACK", 100);
   
      echo VIETJACK;
      print "<br />";
      echo constant("VIETJACK"); // Kết quả được in như lệnh echo bên trên

      print "<br /><br />";
      echo __LINE__;
      print "<br />";
      echo __FILE__;
      print "<br />";
      echo __FUNCTION__;
      print "<br />";
      echo __CLASS__;
      print "<br />";
      echo __METHOD__;
      print "<br />";

      echo true ? 'Ternary operator<br /><br />' : 'B';

      $d = date("D");
         
      if ($d=="Fri")
        echo "Chúc cuối tuần vui vẻ!"; 
      
      elseif ($d=="Sun")
        echo "Chủ nhật vui vẻ!"; 

      else
        echo "Chúc một ngày vui vẻ!"; 

      print "<br />";

      switch ($d)
        {
          case "Mon":
            echo "Hôm nay là Thứ Hai";
            break;
          
          case "Tue":
            echo "Hôm nay là Thứ Ba";
            break;
          
          case "Wed":
            echo "Hôm nay là Thứ Tư";
            break;
          
          case "Thu":
            echo "Hôm nay là Thứ Năm";
            break;
          
          case "Fri":
            echo "Hôm nay là Thứ Sáu";
            break;
          
          case "Sat":
            echo "Hôm nay là Thứ Bảy";
            break;
          
          case "Sun":
            echo "Hôm nay là Chủ Nhật";
            break;
          
          default:
            echo "Còn ngày này là thứ mấy ???";
        }

        $a = 0;
        $b = 0;
        
        for( $i=0; $i<5; $i++ )
        {
          $a += 10;
          $b += 5;
        }
        
        echo ("<br /><br />Sau vòng lặp, giá trị a=$a và b=$b" );

        $i = 0;
        $num = 30;
        
        while( $i < 10)
        {
          $num--;
          $i++;
        }
        
        echo ("<br />Vòng lặp dừng tại giá trị i = $i và num = $num" );

        $j = 0;
         
        do{
          $j++;
        }
        
        while( $j < 10 );
        echo ("<br />Vòng lặp dừng tại giá trị j = $j<br />" );

        $array = array( 1, 2, 3, 4, 5);
         
        foreach( $array as $value )
        {
          echo "Giá trị phần tử mảng là $value <br />";
        }

        $string1 = "String";
        $string2 = "PHP";
        
        echo "<br />" . $string1 . " " . $string2 . "<br />";

        echo strlen("Hoangnq");

        print "<br />";
        echo strpos("Hoangnq","nq");

        function getBrowser()
        { 
          $u_agent = $_SERVER['HTTP_USER_AGENT']; 
          $bname = 'Unknown';
          $platform = 'Unknown';
          $version= "";
          
          //Trước hết, chúng ta kiểm tra nền tảng platform
          if (preg_match('/linux/i', $u_agent)) {
              $platform = 'linux';
          }
          
          elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
              $platform = 'mac';
          }
          
          elseif (preg_match('/windows|win32/i', $u_agent)) {
              $platform = 'windows';
          }
          
          // Tiếp đó, lấy tên của User Agent
          if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
          {
              $bname = 'Internet Explorer';
              $ub = "MSIE";
          }
          
          elseif(preg_match('/Firefox/i',$u_agent))
          {
              $bname = 'Mozilla Firefox';
              $ub = "Firefox";
          }
          
          elseif(preg_match('/Chrome/i',$u_agent))
          {
              $bname = 'Google Chrome';
              $ub = "Chrome";
          }
          
          elseif(preg_match('/Safari/i',$u_agent))
          {
              $bname = 'Apple Safari';
              $ub = "Safari";
          }
          
          elseif(preg_match('/Opera/i',$u_agent))
          {
              $bname = 'Opera';
              $ub = "Opera";
          }
          
          elseif(preg_match('/Netscape/i',$u_agent))
          {
              $bname = 'Netscape';
              $ub = "Netscape";
          }
          
          // Cuối cùng, lấy tên của version
          $known = array('Version', $ub, 'other');
          $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
          
          if (!preg_match_all($pattern, $u_agent, $matches)) {
              // nếu không có so khớp nào, tiếp tục ...
          }
          
          
          $i = count($matches['browser']);
          
          if ($i != 1) {
              
              
              //kiểm tra xem version là trước hay sau
              if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                $version= $matches['version'][0];
              }
              else {
                $version= $matches['version'][1];
              }
          }
          else {
              $version= $matches['version'][0];
          }
          
          
          if ($version==null || $version=="") {$version="?";}
          return array(
              'userAgent' => $u_agent,
              'name'      => $bname,
              'version'   => $version,
              'platform'  => $platform,
              'pattern'   => $pattern
          );
        }
        
        //hiển thị kết quả
        $ua=getBrowser();
        $yourbrowser= "<br />Your browser: " . $ua['name'] . " " . $ua['version'] . " on " .$ua['platform'] . " reports: <br >" . $ua['userAgent'];
        
        print_r($yourbrowser);
        print "<br /><br />";

        try {
          $error = 'Luôn luôn ném lỗi này';
          throw new Exception($error);
          
          // Phần code mà theo sau một ngoại lệ sẽ không được thực thi.
          echo 'Phần code này không bao giờ được thực thi';
        }
        catch (Exception $e) {
            echo 'Bắt ngoại lệ: ',  $e->getMessage(), "\n";
        }
          
        // Tiếp tục tiến trình thực thi
        echo '<br />Hello World<br /><br />';

        print time();
        print "<br />";
        $date_array = getdate();
   
        foreach ( $date_array as $key => $val )
        {
            print "$key = $val<br />";
        }

        $mydate=getdate(date("U")); //bạn tham khảo hàm date() ở bên dưới
        print "Today: ";
        echo "$mydate[weekday], $mydate[month] $mydate[mday], $mydate[year]";


        print "<br /><br />";
        // in ngày trong tuần
        echo date("l") . "<br>";

        // in ngày trong tuần, ngày trong tháng, tháng, năm, thời gian, AM hoặc PM
        echo date("l jS \of F Y h:i:s A") . " ";

        // hiển thị October 3, 1975 là vào Friday
        echo "Oct 3,1975 was on a ".date("l", mktime(0,0,0,10,3,1975)) . "<br>";

        // sử dụng một hằng số trong tham số format
        echo date(DATE_RFC822) . "<br>";

        // hiển thị date time dưới dạng giống như: 1975-10-03T00:00:00+00:00
        echo date(DATE_ATOM,mktime(0,0,0,10,3,1975));

        print "<br /><br />";
        class  Books{
          /* các biến thành viên */
          var $price;
          var $title;
          
          /* các hàm thành viên */
          function setPrice($par){
             $this->price = $par;
          }
          
          function getPrice(){
             echo $this->price ."<br/>";
          }
          
          function setTitle($par){
             $this->title = $par;
          }
          
          function getTitle(){
             echo $this->title ." <br/>";
          }
        }

        $tiengAnh = new Books;
        $toanCaoCap = new Books;
        $tuTuongHCM = new Books;

        $tiengAnh->setTitle( "English Grammar in Use" );
        $tuTuongHCM->setTitle( "Toán cao cấp 1" );
        $toanCaoCap->setTitle( "Tư tưởng Hồ Chí Minh" );

        $tiengAnh->setPrice( 10 );
        $tuTuongHCM->setPrice( 15 );
        $toanCaoCap->setPrice( 7 );

        $tiengAnh->getTitle();
        $tuTuongHCM->getTitle();
        $toanCaoCap->getTitle();
        $tiengAnh->getPrice();
        $tuTuongHCM->getPrice();
        $toanCaoCap->getPrice();

        class Novel extends Books{
          var $publisher;
          
          function setPublisher($par){
             $this->publisher = $par;
          }
          
          function getPublisher(){
             echo $this->publisher. "<br />";
          }
        }

        class MyClass {
          private $car = "skoda";
          var $driver = "SRK";
          
          function __construct($par) {
             // các lệnh ở đây được thực thi mỗi khi
             // một instance của class
             // được tạo
          }
          
          function myPublicFunction() {
             return("Đây là một hàm Public!");
          }
          
          private function myPrivateFunction() {
             return("Đây là một hàm Private!");
          }
        }

        interface Mail {
          public function sendMail();
        }

        class Report implements Mail {
          // lớp này cần định nghĩa hàm sendMail()
          function sendMail() {
            return("sendMail");
          }
        }

        class MyClass2 {
          const requiredMargin = 1.7; // từ khóa const
          
          function __construct($incomingValue) {
             // các lệnh ở đây được thực thi mỗi khi
             // một instance của class
             // được tạo
          }
        }

        abstract class MyAbstractClass {
          abstract function myAbstractFunction();
        }

        class Foo {
          public static $my_static = 'foo';
          
          public function staticValue() {
            return self::$my_static;
          }
        }
        
        print '<br />';
        print Foo::$my_static . "\n";
        $foo = new Foo();
        
        print $foo->staticValue() . "\n";

        class BaseClass {
          public function test() {
            echo "BaseClass::test() called<br>";
          }
          
          final public function moreTesting() {
            echo "BaseClass::moreTesting() called<br>";
          }
        }

        class Name {
          var $_firstName;
          var $_lastName;
          
          function Name($first_name, $last_name) {
            $this->_firstName = $first_name;
            $this->_lastName = $last_name;
          }
          
          function toString() {
            return($this->_lastName .", " .$this->_firstName);
          }
        }
        class NameSub1 extends Name {
          var $_middleInitial;
          
          function NameSub1($first_name, $middle_initial, $last_name) {
            $this->Name::Name($first_name, $last_name);
            $this->_middleInitial = $middle_initial;
          }
          
          function toString() {
            return($this->Name::toString() . " " . $this->_middleInitial);
          }
        }
    ?>
  </div>

  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <script src="js/index.js"></script>
</body>
</html>
